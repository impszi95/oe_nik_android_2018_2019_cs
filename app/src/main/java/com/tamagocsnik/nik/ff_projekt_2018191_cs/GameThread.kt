package com.tamagocsnik.nik.ff_projekt_2018191_cs

import android.graphics.Canvas
import android.view.SurfaceHolder

class GameThread(private val surfaceHolder: SurfaceHolder, private val gameView: GameView) : Thread(){
    private var running: Boolean = false

    private val targetFPS = 60

    fun setRunning(isRunning:Boolean){
        this.running = isRunning
    }

    override fun run() {
        var startTime: Long
        var timeMilliis: Long
        var waitTime: Long
        val targetTime = (1000/targetFPS).toLong()

        while (running){
            startTime = System.nanoTime()
            canvas = null

            try {
                canvas = this.surfaceHolder.lockCanvas()
                synchronized(surfaceHolder){
                    this.gameView.update()
                    this.gameView.draw(canvas!!)
                }
            }catch (e: Exception){
                e.printStackTrace()
            } finally {
                if (canvas != null){
                    try {
                        surfaceHolder.unlockCanvasAndPost(canvas)
                    } catch (e: Exception){
                        e.printStackTrace()
                    }
                }
            }

            timeMilliis = (System.nanoTime() - startTime) / 1000000
            waitTime = targetTime -timeMilliis

            try {
                sleep(waitTime)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    companion object {
        private var canvas: Canvas? = null
    }
}