package com.tamagocsnik.nik.ff_projekt_2018191_cs

import android.content.Context
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Rect
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.SurfaceHolder
import android.view.SurfaceView
import android.widget.Toast
import java.time.Duration
import java.util.jar.Attributes

 class GameView(context: Context, attributes: AttributeSet): SurfaceView(context, attributes),SurfaceHolder.Callback{

     private val thread: GameThread
     private var grenade: Grenade? = null
     private var player: Player? = null

     private var touched: Boolean = false
     private var touched_x: Int = 0
     private var touched_y: Int = 0

     private var collided: Boolean = false

    init {
        holder.addCallback(this)

        thread = GameThread(holder, this)
    }

    override fun surfaceCreated(p0: SurfaceHolder?) {
        grenade = Grenade(BitmapFactory.decodeResource(resources, R.drawable.grenade))
        player = Player(BitmapFactory.decodeResource(resources, R.drawable.player))

        //Toast.makeText(context, "SURFACE CREATED", Toast.LENGTH_SHORT).show()

        thread.setRunning(true)
        thread.start()
    }

    override fun surfaceChanged(p0: SurfaceHolder?, p1: Int, p2: Int, p3: Int) {
    }

    override fun surfaceDestroyed(p0: SurfaceHolder?) {
        var retry = true
        while(retry){
            try{
                thread.setRunning(false)
                thread.join()
            }catch (e: Exception){
                e.printStackTrace()
            }
            retry = false
        }
    }

    fun update(){ //Funcion to update positions of player and game objects
        grenade!!.update()
        if (touched){
            player!!.updateTouch(touched_x, touched_y)
        }

        val levagas = 20
        val grenadeRect: Rect = Rect(grenade!!.x+levagas, grenade!!.y+levagas, grenade!!.x+grenade!!.w-levagas, grenade!!.y + grenade!!.h-levagas)
        val playerRect: Rect = Rect(player!!.x+levagas,  player!!.y+levagas, player!!.x + player!!.w-levagas, player!!.y + player!!.h-levagas)


        if (grenadeRect.intersect(playerRect)){
            collided = true
            thread.setRunning(false)
        }
    }

     override fun draw(canvas: Canvas) { //Everything that has to be drawn on Canvas
         super.draw(canvas)
         grenade!!.draw(canvas)
         player!!.draw(canvas)
     }

     override fun onTouchEvent(event: MotionEvent): Boolean {
         /*  when ever there is a touch on the screen,
             we can get the position of touch which we may use it for track
             which we may use it for tracking some of the game objects
         */
         touched_x = event.x.toInt()
         touched_y = event.y.toInt()

         val action = event.action
         when(action){
             MotionEvent.ACTION_DOWN -> touched = true
             MotionEvent.ACTION_MOVE -> touched = true
             MotionEvent.ACTION_UP -> touched = false
             MotionEvent.ACTION_CANCEL -> touched = false
             MotionEvent.ACTION_OUTSIDE -> touched = false
         }
         return true
     }
}