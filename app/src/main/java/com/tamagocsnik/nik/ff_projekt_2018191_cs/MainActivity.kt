package com.tamagocsnik.nik.ff_projekt_2018191_cs

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.ProgressBar
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_stats.*


class MainActivity: AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {    //onCreate
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //setSupportActionBar(toolbar)


        //Notification bar eltüntetése
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)



        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        displayScreen(-1)


        //BENCE
        //MEMÓRIA INICIALIZÁLÁS




        //IDÁIG
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

//    override fun onCreateOptionsMenu(menu: Menu): Boolean {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        menuInflater.inflate(R.menu.main, menu)
//        return true
//    }



    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)

        }

    }


    //<------------HELP------------->
    //Be kell rakni a when be a megfelelő gombra a kívánt activityt mint ahogy az alatta látszik
    //És meg fogja hívni a kattintáskor!!
    //Annyi még hogy a Létrehoztam egy Main window Activityt amiben a :Fragment től végig kik kell copy és az új activity be bele  (nyilván a R.layout.valami ide át kell írni a lérehozott activity helyére)
    //Így szinte 100 hogy működik

    // Ez hívja meg a menüpontokat! Inventory, Statok
    fun  displayScreen (id: Int){
        val fragment = when (id){
            R.id.nav_statok -> {
                StatsActivity()
            }
            R.id.nav_bolt ->{
                Bolt()
            }
            R.id.nav_masikj ->{
                MasikJ()
            }
            R.id.nav_furdetes->{
                Furdetes()
            }
            else->{
                MainWindow()
            }

        }


        supportFragmentManager.beginTransaction()
                .replace(R.id.constraintlay, fragment)
                .commit()

    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        displayScreen(item.itemId)
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

}
