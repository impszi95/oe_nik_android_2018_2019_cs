package com.tamagocsnik.nik.ff_projekt_2018191_cs

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Canvas

class Grenade(val image: Bitmap) {
    var x: Int = 0
    var y: Int = 0
    var w: Int = 0
    var h: Int = 0
    private var xVelocity = 5
    private var yVelocity = 5
    private val screenWidth = Resources.getSystem().displayMetrics.widthPixels
    private val screenHeight = Resources.getSystem().displayMetrics.heightPixels

    init {
        w = image.width     //kép szélessége
        h = image.height    //kép magassága

        x = screenWidth / 2
        y = screenHeight / 2
    }

    fun draw(canvas: Canvas) { //Draws the object on to the canvas.
        canvas.drawBitmap(image, x.toFloat(), y.toFloat(), null)
    }

    fun update() { //Updates properties for the game object
        if (x > screenWidth - image.width || x < image.width) {
            xVelocity = xVelocity * -1
        }
        if (y > screenHeight - image.height || y < image.height) {
            yVelocity = yVelocity * -1
        }

        x += (xVelocity)
        y += (yVelocity)

    }
}