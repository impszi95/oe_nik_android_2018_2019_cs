package com.tamagocsnik.nik.ff_projekt_2018191_cs

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast

class MainWindow : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.activity_main_window, null)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val btn_etet = view.findViewById<Button>(R.id.btn_etet)
        val txt_szint = view.findViewById<TextView>(R.id.textSzint)
        val txt_xp = view.findViewById<TextView>(R.id.textXP)
        val prb_xp = view.findViewById<ProgressBar>(R.id.progressBarXP)
        val prb_ehseg = view.findViewById<ProgressBar>(R.id.progressBarEhseg)

        val filename = "MyPref"
        val settings = activity?.getSharedPreferences(filename, Context.MODE_PRIVATE)
        val editor = settings!!.edit()


        //KIVENNI
        editor.putInt("Progress_ehseg",10)
        editor.apply()
        //

        var szint = settings!!.getInt("szint",0)
        var xp = settings!!.getInt("xp",0)
        var ehseg = settings!!.getInt("Progress_ehseg",10)


        txt_szint.text = "SZINT: $szint"
        txt_xp.text = "10/ $xp xp"
        prb_xp.progress=xp
        prb_ehseg.progress=ehseg


        btn_etet.setOnClickListener {
            var ehseg = settings!!.getInt("Progress_ehseg", 5)
            if (ehseg > 0) {
                ehseg--;

                xp = settings!!.getInt("xp",0)

                if(xp==9){
                    szint++
                    txt_xp.text="10/ 10 xp"
                    xp=0

                    var kepessegPont = settings!!.getInt("skillpoints",0)
                    kepessegPont++

                    editor.putInt("skillpoints",kepessegPont)
                    editor.apply()

                }else{
                    xp++


                }
                txt_xp.text="10/ $xp xp"
                prb_xp.progress=xp
                editor.putInt("xp",xp)
                txt_szint.text="SZINT: $szint"
                editor.putInt("szint",szint)


                editor.putInt("Progress_ehseg",ehseg)
                editor.apply()

                prb_ehseg.progress = ehseg
                Toast.makeText(activity, "Éhség csökkent. Új: ($ehseg)", Toast.LENGTH_SHORT).show()

            }else{
                Toast.makeText(activity, "Nem vagy éhes.", Toast.LENGTH_SHORT).show()
            }
        }
    }
}
