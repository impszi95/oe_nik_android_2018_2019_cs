package com.tamagocsnik.nik.ff_projekt_2018191_cs

import android.content.Context.MODE_PRIVATE
import android.os.Bundle

import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import kotlinx.android.synthetic.*
import kotlinx.android.synthetic.main.content_stats.*
import kotlinx.android.synthetic.main.content_stats.view.*

class StatsActivity : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {



        return inflater?.inflate(R.layout.activity_stats, null)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //Beolvasás
        val filename = "MyPref"
        val settings = activity?.getSharedPreferences(filename, MODE_PRIVATE)
            //Berak
        var SkillPoints = settings!!.getInt("skillpoints", 0)


        skillpointsText.text = SkillPoints.toString()
        val bt1 = view.findViewById<Button>(R.id.button_ugy)
        val pr1 = view.findViewById<ProgressBar>(R.id.progressBar)
        val bt2 = view.findViewById<Button>(R.id.button_gyors)
        val pr2 = view.findViewById<ProgressBar>(R.id.progressBar9)
        val bt3 = view.findViewById<Button>(R.id.button_reak)
        val pr3 = view.findViewById<ProgressBar>(R.id.progressBar8)
        val bt4 = view.findViewById<Button>(R.id.button_ero)
        val pr4 = view.findViewById<ProgressBar>(R.id.progressBar7)
        val pr5 = view.findViewById<ProgressBar>(R.id.progressBar6)

        pr1.progress = settings!!.getInt("Progress_ugyesseg", 0)
        pr2.progress = settings!!.getInt("Progress_gyorsasag", 0)
        pr3.progress = settings!!.getInt("Progress_reakcioido", 0)
        pr4.progress = settings!!.getInt("Progress_ero", 0)
        pr5.progress = settings!!.getInt("Progress_ehseg", 0)

        //Init Tárolás
        val editor = settings.edit()



        bt1.setOnClickListener {
            if (SkillPoints > 0) {
                pr1.progress++
                SkillPoints -= 1
                skillpointsText.text = SkillPoints.toString()
                //Tárolás
                editor.putInt("skillpoints", SkillPoints)
                editor.apply()
                editor.putInt("Progress_ugyesseg", pr1.progress)
                editor.apply()
            }
        }
        bt2.setOnClickListener {
            if (SkillPoints > 0) {
                pr2.progress++
                SkillPoints -= 1
                skillpointsText.text = SkillPoints.toString()
                //Tárolás
                editor.putInt("skillpoints", SkillPoints)
                editor.apply()
                editor.putInt("Progress_gyorsasag", pr2.progress)
                editor.apply()
            }
        }
        bt3.setOnClickListener {
            if (SkillPoints > 0) {
                pr3.progress++
                SkillPoints -= 1
                skillpointsText.text = SkillPoints.toString()
                //Tárolás
                editor.putInt("skillpoints", SkillPoints)
                editor.apply()
                editor.putInt("Progress_reakcioido", pr3.progress)
                editor.apply()
            }
        }
        bt4.setOnClickListener {
            if (SkillPoints > 0) {
                pr4.progress++
                SkillPoints -= 1
                skillpointsText.text = SkillPoints.toString()
                //Tárolás
                editor.putInt("skillpoints", SkillPoints)
                editor.apply()
                editor.putInt("Progress_ero", pr4.progress)
                editor.apply()
            }
        }
    }
}
