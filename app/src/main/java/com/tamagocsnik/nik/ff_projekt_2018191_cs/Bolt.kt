package com.tamagocsnik.nik.ff_projekt_2018191_cs

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast

class Bolt : Fragment(){

    private var mTextViewEmpty: TextView? = null
    private var mProgressBarLoading: ProgressBar? = null
    private var mImageViewEmpty: ImageView? = null
    private var mRecyclerView: RecyclerView? = null
    private var mListadapter: ListAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.activity_bolt, container, false)

        mRecyclerView = view.findViewById(R.id.recyclerView)
        mTextViewEmpty = view.findViewById(R.id.textViewEmpty)
        mImageViewEmpty = view.findViewById(R.id.imageViewEmpty)
        mProgressBarLoading = view.findViewById(R.id.progressBarLoading)

        val layoutManager = LinearLayoutManager(activity)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        mRecyclerView!!.layoutManager = layoutManager

        val data = ArrayList<DataNote>()
        for (i in DataNoteInformation.id.indices) {
            data.add(
                    DataNote(
                            DataNoteInformation.id[i],
                            DataNoteInformation.textArray[i],
                            DataNoteInformation.dateArray[i]
                    ))
        }

        mListadapter = ListAdapter(data)
        mRecyclerView!!.adapter = mListadapter

        return view
    }

    inner class ListAdapter(private val dataList: ArrayList<DataNote>) : RecyclerView.Adapter<ListAdapter.ViewHolder>() {

        inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            internal var textViewText: TextView = itemView.findViewById<View>(R.id.text) as TextView
            internal var textViewComment: TextView = itemView.findViewById<View>(R.id.comment) as TextView
            internal var textViewDate: TextView = itemView.findViewById<View>(R.id.date) as TextView

        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListAdapter.ViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_layout, parent, false)

            return ViewHolder(view)
        }

        override fun onBindViewHolder(holder: ListAdapter.ViewHolder, position: Int) {
            holder.textViewText.text = dataList[position].text
            holder.textViewComment.text = dataList[position].comment
            holder.textViewDate.text = dataList[position].date

            holder.itemView.setOnClickListener { //Beolvasás
                val filename = "MyPref"
                val settings = activity?.getSharedPreferences(filename, Context.MODE_PRIVATE)
                //Level
                var szint = settings!!.getInt("szint", 0)
                var xp = settings!!.getInt("xp", 0)

                val editor = settings!!.edit()
                if (xp == 9)
                {
                    //Init Tárolás
                    szint = szint + 1
                    editor.putInt("szint",szint)
                    editor.apply()
                    editor.putInt("xp", 1)
                    editor.apply()
                    var skillpoints = settings!!.getInt("skillpoints", 0)
                    skillpoints = skillpoints+1
                    editor.putInt("skillpoints", skillpoints)
                    editor.apply()
                }
                else
                {
                    xp = xp + 1
                    editor.putInt("xp", xp)
                    editor.apply()
                }
                xp = settings!!.getInt("xp", 0)
                Toast.makeText(activity, "Meglévő XP:" + xp.toString(), Toast.LENGTH_SHORT).show() }
        }

        override fun getItemCount(): Int {
            return dataList.size
        }
    }

}

object DataNoteInformation {
    var textArray = arrayOf("Alma", "Sertés hús", "Banán" , "Körte", "Szalonna", "Kocsi", "Ékszer", "Ruha", "Cipő", "Pizza")
    var dateArray = arrayOf("1","1","1","1","1","1","1","1","1","1")
    var id = arrayOf("1","2","3","4","5","6","7","8","9","10")

}

class DataNote(text: String, comment: String, date: String) {
    var text: String
        internal set
    var comment: String
        internal set
    var date: String
        internal set

    init {
        this.text = text
        this.comment = comment
        this.date = date
    }
}